<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog From Scratch</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    
    <?php include "../includes/header.php" ?>

<?php

ini_set('display_errors','on');
error_reporting(E_ALL);

// Se connecter à mysql
$bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8', 'pauline', 'pauline');

//Ma requête sql pour afficher l'auteur dans l'article

$reponse = $bdd->query(
    'SELECT articles.id, articles.title, articles.content, articles.image_url, articles.published_at, 
    articles.reading_time, authors.firstname, authors.lastname 
    FROM articles
    JOIN authors ON articles.author_id = authors.id 
    ORDER BY published_at DESC'
);


// On affiche chaque entrée une à une
while ($donnees = $reponse->fetch())
{

?>
<article class="article">
    <div class="titrearticle">
    <strong><?php echo $donnees['title'];?></strong><br />
    </div>
        <?php echo strip_tags(substr($donnees['content'], 0, 300))?>;<br/>
        <img src= <?php echo $donnees['image_url']; ?>><br/>
        <div class="datepub">
        <strong>Date de publication: </strong><?php echo $donnees['published_at']; ?><br />
        </div>
        <strong>Le temps de lecture:</strong><?php echo $donnees['reading_time']; ?> <br />
        <strong>L'auteur: </strong><?php echo $donnees['firstname'] . ' ' .$donnees['lastname'] ?><br /> <br />
    <?php    
        $articleId = $donnees["id"];
        $categories =$bdd->query( // faire une requête afin d'afficher les catégories
        'SELECT categories.category 
        FROM articles_categories
        JOIN categories ON articles_categories.category_id=categories.id 
        WHERE article_id = '.$articleId.' ' // on filtre car c'est par rapport à un seul article
        );
        ?>
        <p> Catégorie : </p>
        <?php
        // faire une boucle afin d'afficher toute les catégories
            while($categorie =$categories->fetch())
            {
            echo $categorie['category']."<br />";
             
            }
    ?>
            </article>
            <?php
$categories->closeCursor();
}

$reponse->closeCursor(); // Termine le traitement de la requête
?>

<?php include "../includes/footer.php" ?>

</body>
</html>

